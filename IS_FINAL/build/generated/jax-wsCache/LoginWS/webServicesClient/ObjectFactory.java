
package webServicesClient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webServicesClient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateDB_QNAME = new QName("http://WebServices/", "CreateDB");
    private final static QName _InsertAdminResponse_QNAME = new QName("http://WebServices/", "InsertAdminResponse");
    private final static QName _UpdateCustomerToUserResponse_QNAME = new QName("http://WebServices/", "UpdateCustomerToUserResponse");
    private final static QName _InsertAdmin_QNAME = new QName("http://WebServices/", "InsertAdmin");
    private final static QName _CreateDBResponse_QNAME = new QName("http://WebServices/", "CreateDBResponse");
    private final static QName _InsertCustomer_QNAME = new QName("http://WebServices/", "InsertCustomer");
    private final static QName _UpdateCustomerToUser_QNAME = new QName("http://WebServices/", "UpdateCustomerToUser");
    private final static QName _GetRoleResponse_QNAME = new QName("http://WebServices/", "getRoleResponse");
    private final static QName _GetRole_QNAME = new QName("http://WebServices/", "getRole");
    private final static QName _UpdateCustomerToAdminResponse_QNAME = new QName("http://WebServices/", "UpdateCustomerToAdminResponse");
    private final static QName _InsertCustomerResponse_QNAME = new QName("http://WebServices/", "InsertCustomerResponse");
    private final static QName _UpdateCustomerToAdmin_QNAME = new QName("http://WebServices/", "UpdateCustomerToAdmin");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webServicesClient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateDB }
     * 
     */
    public CreateDB createCreateDB() {
        return new CreateDB();
    }

    /**
     * Create an instance of {@link InsertAdminResponse }
     * 
     */
    public InsertAdminResponse createInsertAdminResponse() {
        return new InsertAdminResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerToUserResponse }
     * 
     */
    public UpdateCustomerToUserResponse createUpdateCustomerToUserResponse() {
        return new UpdateCustomerToUserResponse();
    }

    /**
     * Create an instance of {@link CreateDBResponse }
     * 
     */
    public CreateDBResponse createCreateDBResponse() {
        return new CreateDBResponse();
    }

    /**
     * Create an instance of {@link InsertCustomer }
     * 
     */
    public InsertCustomer createInsertCustomer() {
        return new InsertCustomer();
    }

    /**
     * Create an instance of {@link InsertAdmin }
     * 
     */
    public InsertAdmin createInsertAdmin() {
        return new InsertAdmin();
    }

    /**
     * Create an instance of {@link UpdateCustomerToUser }
     * 
     */
    public UpdateCustomerToUser createUpdateCustomerToUser() {
        return new UpdateCustomerToUser();
    }

    /**
     * Create an instance of {@link GetRole }
     * 
     */
    public GetRole createGetRole() {
        return new GetRole();
    }

    /**
     * Create an instance of {@link GetRoleResponse }
     * 
     */
    public GetRoleResponse createGetRoleResponse() {
        return new GetRoleResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerToAdminResponse }
     * 
     */
    public UpdateCustomerToAdminResponse createUpdateCustomerToAdminResponse() {
        return new UpdateCustomerToAdminResponse();
    }

    /**
     * Create an instance of {@link InsertCustomerResponse }
     * 
     */
    public InsertCustomerResponse createInsertCustomerResponse() {
        return new InsertCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerToAdmin }
     * 
     */
    public UpdateCustomerToAdmin createUpdateCustomerToAdmin() {
        return new UpdateCustomerToAdmin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "CreateDB")
    public JAXBElement<CreateDB> createCreateDB(CreateDB value) {
        return new JAXBElement<CreateDB>(_CreateDB_QNAME, CreateDB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "InsertAdminResponse")
    public JAXBElement<InsertAdminResponse> createInsertAdminResponse(InsertAdminResponse value) {
        return new JAXBElement<InsertAdminResponse>(_InsertAdminResponse_QNAME, InsertAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerToUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateCustomerToUserResponse")
    public JAXBElement<UpdateCustomerToUserResponse> createUpdateCustomerToUserResponse(UpdateCustomerToUserResponse value) {
        return new JAXBElement<UpdateCustomerToUserResponse>(_UpdateCustomerToUserResponse_QNAME, UpdateCustomerToUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "InsertAdmin")
    public JAXBElement<InsertAdmin> createInsertAdmin(InsertAdmin value) {
        return new JAXBElement<InsertAdmin>(_InsertAdmin_QNAME, InsertAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDBResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "CreateDBResponse")
    public JAXBElement<CreateDBResponse> createCreateDBResponse(CreateDBResponse value) {
        return new JAXBElement<CreateDBResponse>(_CreateDBResponse_QNAME, CreateDBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "InsertCustomer")
    public JAXBElement<InsertCustomer> createInsertCustomer(InsertCustomer value) {
        return new JAXBElement<InsertCustomer>(_InsertCustomer_QNAME, InsertCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerToUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateCustomerToUser")
    public JAXBElement<UpdateCustomerToUser> createUpdateCustomerToUser(UpdateCustomerToUser value) {
        return new JAXBElement<UpdateCustomerToUser>(_UpdateCustomerToUser_QNAME, UpdateCustomerToUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getRoleResponse")
    public JAXBElement<GetRoleResponse> createGetRoleResponse(GetRoleResponse value) {
        return new JAXBElement<GetRoleResponse>(_GetRoleResponse_QNAME, GetRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getRole")
    public JAXBElement<GetRole> createGetRole(GetRole value) {
        return new JAXBElement<GetRole>(_GetRole_QNAME, GetRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerToAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateCustomerToAdminResponse")
    public JAXBElement<UpdateCustomerToAdminResponse> createUpdateCustomerToAdminResponse(UpdateCustomerToAdminResponse value) {
        return new JAXBElement<UpdateCustomerToAdminResponse>(_UpdateCustomerToAdminResponse_QNAME, UpdateCustomerToAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "InsertCustomerResponse")
    public JAXBElement<InsertCustomerResponse> createInsertCustomerResponse(InsertCustomerResponse value) {
        return new JAXBElement<InsertCustomerResponse>(_InsertCustomerResponse_QNAME, InsertCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerToAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "UpdateCustomerToAdmin")
    public JAXBElement<UpdateCustomerToAdmin> createUpdateCustomerToAdmin(UpdateCustomerToAdmin value) {
        return new JAXBElement<UpdateCustomerToAdmin>(_UpdateCustomerToAdmin_QNAME, UpdateCustomerToAdmin.class, null, value);
    }

}
