
package webServicesClient3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webServicesClient3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetProgamsResponse_QNAME = new QName("http://WebServices/", "getProgamsResponse");
    private final static QName _OperateDeviceResponse_QNAME = new QName("http://WebServices/", "operateDeviceResponse");
    private final static QName _AssociateDevice_QNAME = new QName("http://WebServices/", "AssociateDevice");
    private final static QName _GetMyDevicesResponse_QNAME = new QName("http://WebServices/", "getMyDevicesResponse");
    private final static QName _GetallTypes_QNAME = new QName("http://WebServices/", "getallTypes");
    private final static QName _GetallTypesResponse_QNAME = new QName("http://WebServices/", "getallTypesResponse");
    private final static QName _OperateDevice_QNAME = new QName("http://WebServices/", "operateDevice");
    private final static QName _GetMyDevices_QNAME = new QName("http://WebServices/", "getMyDevices");
    private final static QName _GetProgams_QNAME = new QName("http://WebServices/", "getProgams");
    private final static QName _AssociateDeviceResponse_QNAME = new QName("http://WebServices/", "AssociateDeviceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webServicesClient3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetProgamsResponse }
     * 
     */
    public GetProgamsResponse createGetProgamsResponse() {
        return new GetProgamsResponse();
    }

    /**
     * Create an instance of {@link AssociateDevice }
     * 
     */
    public AssociateDevice createAssociateDevice() {
        return new AssociateDevice();
    }

    /**
     * Create an instance of {@link GetMyDevicesResponse }
     * 
     */
    public GetMyDevicesResponse createGetMyDevicesResponse() {
        return new GetMyDevicesResponse();
    }

    /**
     * Create an instance of {@link OperateDeviceResponse }
     * 
     */
    public OperateDeviceResponse createOperateDeviceResponse() {
        return new OperateDeviceResponse();
    }

    /**
     * Create an instance of {@link GetallTypes }
     * 
     */
    public GetallTypes createGetallTypes() {
        return new GetallTypes();
    }

    /**
     * Create an instance of {@link GetallTypesResponse }
     * 
     */
    public GetallTypesResponse createGetallTypesResponse() {
        return new GetallTypesResponse();
    }

    /**
     * Create an instance of {@link OperateDevice }
     * 
     */
    public OperateDevice createOperateDevice() {
        return new OperateDevice();
    }

    /**
     * Create an instance of {@link AssociateDeviceResponse }
     * 
     */
    public AssociateDeviceResponse createAssociateDeviceResponse() {
        return new AssociateDeviceResponse();
    }

    /**
     * Create an instance of {@link GetMyDevices }
     * 
     */
    public GetMyDevices createGetMyDevices() {
        return new GetMyDevices();
    }

    /**
     * Create an instance of {@link GetProgams }
     * 
     */
    public GetProgams createGetProgams() {
        return new GetProgams();
    }

    /**
     * Create an instance of {@link TypeModel }
     * 
     */
    public TypeModel createTypeModel() {
        return new TypeModel();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProgamsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getProgamsResponse")
    public JAXBElement<GetProgamsResponse> createGetProgamsResponse(GetProgamsResponse value) {
        return new JAXBElement<GetProgamsResponse>(_GetProgamsResponse_QNAME, GetProgamsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperateDeviceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "operateDeviceResponse")
    public JAXBElement<OperateDeviceResponse> createOperateDeviceResponse(OperateDeviceResponse value) {
        return new JAXBElement<OperateDeviceResponse>(_OperateDeviceResponse_QNAME, OperateDeviceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssociateDevice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "AssociateDevice")
    public JAXBElement<AssociateDevice> createAssociateDevice(AssociateDevice value) {
        return new JAXBElement<AssociateDevice>(_AssociateDevice_QNAME, AssociateDevice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMyDevicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getMyDevicesResponse")
    public JAXBElement<GetMyDevicesResponse> createGetMyDevicesResponse(GetMyDevicesResponse value) {
        return new JAXBElement<GetMyDevicesResponse>(_GetMyDevicesResponse_QNAME, GetMyDevicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetallTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getallTypes")
    public JAXBElement<GetallTypes> createGetallTypes(GetallTypes value) {
        return new JAXBElement<GetallTypes>(_GetallTypes_QNAME, GetallTypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetallTypesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getallTypesResponse")
    public JAXBElement<GetallTypesResponse> createGetallTypesResponse(GetallTypesResponse value) {
        return new JAXBElement<GetallTypesResponse>(_GetallTypesResponse_QNAME, GetallTypesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OperateDevice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "operateDevice")
    public JAXBElement<OperateDevice> createOperateDevice(OperateDevice value) {
        return new JAXBElement<OperateDevice>(_OperateDevice_QNAME, OperateDevice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMyDevices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getMyDevices")
    public JAXBElement<GetMyDevices> createGetMyDevices(GetMyDevices value) {
        return new JAXBElement<GetMyDevices>(_GetMyDevices_QNAME, GetMyDevices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProgams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "getProgams")
    public JAXBElement<GetProgams> createGetProgams(GetProgams value) {
        return new JAXBElement<GetProgams>(_GetProgams_QNAME, GetProgams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssociateDeviceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebServices/", name = "AssociateDeviceResponse")
    public JAXBElement<AssociateDeviceResponse> createAssociateDeviceResponse(AssociateDeviceResponse value) {
        return new JAXBElement<AssociateDeviceResponse>(_AssociateDeviceResponse_QNAME, AssociateDeviceResponse.class, null, value);
    }

}
