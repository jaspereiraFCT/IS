<%-- 
    Document   : index.jsp
    Created on : May 1, 2014, 3:36:09 PM
    Author     : Joaquim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>IS RJJ - Enterprise Manager Version 0.1</title>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link href="../css/dashboard.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <%
            String UserName = "";
            String role = "";
            if (session.getAttribute("user") == null || session.getAttribute("role") == null) {
                response.sendRedirect("../login.jsp");
            } else {
                UserName = session.getAttribute("user").toString();
                role = session.getAttribute("role").toString();
            }
        %>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="../home.jsp">IS Manager</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="../home.jsp">Home</a></li>
                        <li><a href="#" id="logoutBT">Logout (<% out.print(UserName);%>)</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">

                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="ClientSide.jsp">My Account</a></li>
                            <%if (role.equalsIgnoreCase("admin")) { %>
                        <li><a href="EnterpriseSide.jsp">Enterprise</a></li>
                            <%}%>
                    </ul>
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <div id="resultDiv" class="row"/>
                    <h1 class="page-header">My Account</h1>
                    <div id="resultDiv" style="visibility: hidden"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Associar Electrodoméstico</h3>
                                </div>
                                <div class="panel-body">
                                    <button id="associarBT" type="button" class="btn btn-primary">Associar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Operar Electrodoméstico</h3>
                                </div>
                                <div class="panel-body">
                                    <button type="button" class="btn btn-success" id="operarBT">Operar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2 class="sub-header">Electrodomésticos Associados</h2>
                    <div id="resultelectro" class="row table-responsive"></div>
                </div>
            </div>
        </div>





        <div id="associarDiv" class="login-panel panel panel-default" style="display : none">
            <div class="panel-heading">
                <h3 class="panel-title">Associar</h3>
            </div>
            <div class="panel-body">
                <form id="associateForm" method="POST" action="../ClientServlet">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Serial" name="SerialC" type="text" autofocus="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Model" name="modelC" type="text" value="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="FrindlyName" name="FrindlyNameC" type="text" value="" required="">
                        </div>
                        <div class="form-group" style="display:none">
                            <input name="opt" type="text" value="associar">
                        </div>
                        <div class="form-group">
                            <select id="ddDeviceType" class="form-control" name="typeUID" required="">
                            </select>
                        </div>
                        <button class="btn btn-lg btn-success btn-block" name="btAssociar" type="submit">Associar</button>
                    </fieldset>
                </form>
            </div>
        </div>
                    
        <div id="operarDiv" class="login-panel panel panel-default" style="display : none">
            <div class="panel-heading">
                <h3 class="panel-title">Associar</h3>
            </div>
            <div class="panel-body">
                <form id="operarForm" method="POST" action="../ClientServlet">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Serial Number" name="SerialNumberO" type="text" autofocus="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Temperatura" name="tempO" type="number" value="" required="">
                        </div>
                        <div class="form-group" style="display:none">
                            <input name="opt" type="text" value="operar">
                        </div>
                        <div class="form-group">
                            <select id="ddprograms" class="form-control" name="ddprograms" required="">
                            </select>
                        </div>
                        <button class="btn btn-lg btn-success btn-block" name="btAssociar" type="submit">Operar</button>
                    </fieldset>
                </form>
            </div>
        </div>

        <script src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="../js/jquery.lightbox_me.js" type="text/javascript"></script>
        <script src="js/ClientJS.js" type="text/javascript"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/logoutJS.js" type="text/javascript"></script>
    </body>
</html>
