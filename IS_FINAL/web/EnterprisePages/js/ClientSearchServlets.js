$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: '../EnterpriseServlet',
        data: {btSelect: "getRoles"},
        success: function(rjson) {
            $('#roleDD').empty();
            $.each(rjson, function(index, user) {
                $('#roleDD').append($('<option>').text(user.role).attr('value', user.id));
            });
        }
    });

    $('#changeRoleForm').submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: $('#changeRoleForm').attr('method'),
            url: $('#changeRoleForm').attr('action'),
            data: $('#changeRoleForm').serialize(),
            success: function(response) {
                if (response.toString().search("NOT") !== -1) {
                    $('#resultDiv2').text(response).css('visibility', 'visible').addClass('alert alert-danger');
                } else {
                    $('#resultDiv2').text(response).css('visibility', 'visible').addClass('alert alert-success');
                }
            }
        });
    });

    $('#dbGenerate').submit(function(event) {
        event.preventDefault();
        $.ajax({
            type: $('#dbGenerate').attr('method'),
            url: $('#dbGenerate').attr('action'),
            data: $('#dbGenerate').serialize(),
            success: function(response) {
                if (response.toString().search("NOT") !== -1) {
                    $('#resultDiv').text(response).css('visibility', 'visible').addClass('alert alert-danger');
                } else {
                    $('#resultDiv').text(response).css('visibility', 'visible').addClass('alert alert-success');
                }
            }
        });
    });

    $('#searchForm').submit(function(event) {
        event.preventDefault();

        $.ajax({
            type: $('#searchForm').attr('method'),
            url: $('#searchForm').attr('action'),
            data: $('#searchForm').serialize(),
            success: function(responseJson) { // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
                $('#searchtab').remove();
                var $table = $('<table>').appendTo($('#tableSearch')).addClass('table table-striped').attr("id", 'searchtab'); // Create HTML <table> element and append it to HTML DOM element with ID "somediv".
                $table.append("<thead><tr>")
                        .append("<th>Serial Number</th>")
                        .append("<th>Type</th>")
                        .append("<th>Program</th>")
                        .append("<th>Temperature</th>")
                        .append("<th>Date</th></tr></thead>");
                $.each(responseJson, function(index, user) {    // Iterate over the JSON array.
                    $('<tr>').appendTo($table)                     // Create HTML <tr> element, set its text content with currently iterated item and append it to the <table>.
                            .append($('<td>').text(user.serial_number))        // Create HTML <td> element, set its text content with id of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.type_description))      // Create HTML <td> element, set its text content with name of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.program_description)) // Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.temp)) // Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.date));    // Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.
                });
            }
        });
    });

    $('#searchFormSR').submit(function(event) {
        event.preventDefault();

        $.ajax({
            type: $('#searchFormSR').attr('method'),
            url: $('#searchFormSR').attr('action'),
            data: $('#searchFormSR').serialize(),
            success: function(responseJson) { // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
                $('#searchtab2').remove();
                var $table = $('<table>').appendTo($('#tableSearchElectro')).addClass('table table-striped').attr("id", 'searchtab2'); // Create HTML <table> element and append it to HTML DOM element with ID "somediv".
                $table.append("<thead><tr>")
                        .append("<th>Time</th>")
                        .append("<th>Program</th>")
                        .append("<th>Temperature</th>");
                $.each(responseJson, function(index, user) {    // Iterate over the JSON array.
                    $('<tr>').appendTo($table)                     // Create HTML <tr> element, set its text content with currently iterated item and append it to the <table>.
                            .append($('<td>').text(user.time))        // Create HTML <td> element, set its text content with id of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.program_description))      // Create HTML <td> element, set its text content with name of currently iterated product and append it to the <tr>.
                            .append($('<td>').text(user.water_temperature)); // Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.   // Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.
                });
            }
        });
    });
});


