<%-- 
    Document   : login
    Created on : May 1, 2014, 3:00:03 PM
    Author     : Joaquim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link href="css/login.css" rel="stylesheet" type="text/css" >
        <title>IS - Enterprise Login Page</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="LoginPageServlet">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="UserName" name="user"  autofocus="" required="">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="pwd" type="password" value="" required="">
                                    </div>

                                    <button class="btn btn-lg btn-success btn-block" name="btLogin" value="Enter" type="submit">Sign in</button>
                                    <button class="btn btn-lg btn-primary btn-block" id="accountBT" name="crateAccount" type="submit">Create Account</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="crtAcount" class="login-panel panel panel-default" style="display : none">
            <div class="panel-heading">
                <h3 class="panel-title">Create Account</h3>
            </div>
            <div class="panel-body">
                <form method="POST" action="LoginPageServlet">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="UserName" name="userNameC"  autofocus="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Name" name="userC" type="text" autofocus="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="pwdC" type="password" value="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Telemóvel" name="telC"  pattern="\d{9}" type="text" value="" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Residencia" name="resiC" type="text" value="" required="">
                        </div>

                        <button class="btn btn-lg btn-success btn-block" name="btLogin" value="Create" type="submit">Create</button>
                    </fieldset>
                </form>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/jquery.lightbox_me.js" type="text/javascript"></script>
        <script src="js/LoginJS.js" type="text/javascript"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    </body>
</html>
