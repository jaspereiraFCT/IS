package Controllers;

public class LoginController {
    private static final webServicesClient.LoginWS_Service service = new webServicesClient.LoginWS_Service();
    private static final webServicesClient.LoginWS port = service.getLoginWSPort();
    
    public static boolean createDB() {
        return port.createDB();
    }

    public static Boolean insertAdmin(String userName, String name, int telephone, String residence,String password) {
        return port.insertAdmin(userName, name, telephone, residence, password);
    }

    public static Boolean insertCustomer(String userName, String name, int telephone, String residence,String password) {
        return port.insertCustomer(userName, name, telephone, residence,password);
    }

    public static Boolean updateCustomerToAdmin(String userName) {
        return port.updateCustomerToAdmin(userName);
    }

    public static Boolean updateCustomerToUser(String userName) {
        return port.updateCustomerToUser(userName);
    }

    public static String getRole(String userName, String pw) {
        return port.getRole(userName, pw);
    }

}
