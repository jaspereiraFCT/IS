/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controllers.Clients;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Joaquim
 */
public class ClientServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        String user = session.getAttribute("user").toString();
        String opt = request.getParameter("opt");
        String json = "";

        switch (opt) {
            case "default":
                json = new Gson().toJson(Clients.structMyDevices(Clients.getMyDevices(user)));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            case "getTypes":
                json = new Gson().toJson(Clients.getallTypes());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            case "getPrograms":
                json = new Gson().toJson(Clients.structPrograms(Clients.getProgams()));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            default:
                break;
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String user = session.getAttribute("user").toString();
        String opt = request.getParameter("opt");
        String text = "";
        
        switch (opt) {
            case "associar":
                try {
                    int SerialC = Integer.parseInt(request.getParameter("SerialC"));
                    String modelC = request.getParameter("modelC");
                    String FrindlyNameC = request.getParameter("FrindlyNameC");
                    int typeUID = Integer.parseInt(request.getParameter("typeUID"));
                    if (Clients.associateDevice(SerialC, user, modelC, FrindlyNameC, typeUID)) {
                        text = "Associated!!";
                    } else {
                        text = "NOT Associated!!";
                    }
                } catch (Exception e) {
                    text = "NOT Associated!!";
                }

                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(text);
                break;
            case "operar":
                try {
                    int SerialN = Integer.parseInt(request.getParameter("SerialNumberO"));
                    int temp = Integer.parseInt(request.getParameter("tempO"));
                    int id = Integer.parseInt(request.getParameter("ddprograms"));
                    
                    if (Clients.operateDevice(SerialN, id,temp)) {
                        text = "Machine with S/N: "+ SerialN + " started with program "+id+ " with Temp: " + temp + " ºC";
                    } else {
                        text = "Machine with S/N: "+ SerialN + " did NOT started!!";
                    }
                } catch (Exception e) {
                    text = "Some Probleam with the Server";
                }

                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(text);
                break;
            default:
                break;
        }
    }

}
