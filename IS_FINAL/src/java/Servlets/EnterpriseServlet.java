/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Controllers.Clients;
import Controllers.DataBase;
import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Joaquim
 */
public class EnterpriseServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String text = "";
        String json = "";
        String opt = (request.getParameter("btSelect") == null) ? "" : request.getParameter("btSelect");
        String UserName = request.getParameter("User_Name");
        String SerialNumber = request.getParameter("Serial_Number");

        switch (opt) {
            case "SearchUser":
                json = new Gson().toJson(Clients.convertFromStringArray(Clients.getCustomerUsages(UserName)));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            case "SearchSerial":
                json = new Gson().toJson(Clients.convert2FromStringArray(Clients.getDeviceUsages(SerialNumber)));
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            case "DB":
                if (DataBase.CreateDB()) {
                    text = "DataBase Created With Success!!";
                } else {
                    text = "DataBase NOT Created With Success!!";
                }

                response.setContentType("text/plain");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(text);
                break;
            case "getRoles":
                json = new Gson().toJson(Clients.getAllRoles());
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                break;
            default:
                System.out.println("DEFUALT");
                String encodedURL = response.encodeRedirectURL("EnterprisePages/EnterpriseSide.jsp");
                response.sendRedirect(encodedURL);
                break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = "";
        String UserName = req.getParameter("UserNameChoose");
        String role = req.getParameter("roleChoose");
        if (Clients.updateUserRole(UserName, Integer.parseInt(role))) {
            text = "Changed with Success!!";
        } else {
            text = "NOT Changed!!";
        }

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(text);
    }

}
