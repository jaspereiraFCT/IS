package Servlets;

import Controllers.LoginController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Joaquim
 */
public class LoginPageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String opt = request.getParameter("btLogin");
        String user = request.getParameter("user");

        switch (opt) {
            case "Enter":

                String Role = LoginController.getRole(user, request.getParameter("pwd"));

                if (Role != null) {
                    HttpSession session = request.getSession();

                    session.setAttribute("user", user);
                    session.setAttribute("role", Role);
                    session.setMaxInactiveInterval(60 * 30);
                    String encodedURL = response.encodeRedirectURL("home.jsp");
                    response.sendRedirect(encodedURL);
                } else {
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                    PrintWriter out = response.getWriter();
                    out.println(
                            "<div class=\"row\">"
                            + "<div class=\"col-md-4 col-md-offset-4\">"
                            + "<div class=\"alert alert-warning\">Either user name or password is wrong.</div>"
                            + "</div></div>");
                    rd.include(request, response);
                }

                break;
            case "Create":

                String userNameC = request.getParameter("userNameC");
                String userC = request.getParameter("userC");
                String pwdC = request.getParameter("pwdC");
                String telC = request.getParameter("telC");
                String resiC = request.getParameter("resiC");
                
                boolean control = LoginController.insertCustomer(userNameC, userC, Integer.parseInt(telC), resiC, pwdC);
                
                if (control) {
                    HttpSession session = request.getSession();
                    session.setAttribute("user", userNameC);
                    session.setAttribute("role", "user");
                    session.setMaxInactiveInterval(60 * 30);
                    String encodedURL = response.encodeRedirectURL("home.jsp");
                    response.sendRedirect(encodedURL);
                } else {
                    RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                    PrintWriter out = response.getWriter();
                    out.println(
                            "<div class=\"row\">"
                            + "<div class=\"col-md-4 col-md-offset-4\">"
                            + "<div class=\"alert alert-warning\">Cannot Create a User!</div>"
                            + "</div></div>");
                    rd.include(request, response);
                }

                break;
            default:
                String encodedURL = response.encodeRedirectURL("/login.jsp");
                response.sendRedirect(encodedURL);
                break;
        }
    }

}
