package DataBase;

import Models.RolesModel;
import Models.TypeModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseManagement {

    private Connection connection;
    private Statement statement;

    private boolean openConnection() {
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection("jdbc:h2:../database/CloudDB;", "sa", "");
            statement = statement = connection.createStatement();
            return true;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private boolean closeConnection() {
        try {
            connection.close();
            statement.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean createDB() {
        try {

            openConnection();

            statement.execute("drop table if exists PUBLIC.CUSTOMER;");
            statement.execute("drop table if exists PUBLIC.DEVICE;");
            statement.execute("drop table if exists PUBLIC.TYPE;");
            statement.execute("drop table if exists PUBLIC.DEVICE_USAGE;");
            statement.execute("drop table if exists PUBLIC.PROGRAM;");
            statement.execute("drop table if exists PUBLIC.LOGINROLES;");

            statement.execute("create table PUBLIC.LOGINROLES ("
                    + "LOGINROLES_ID INTEGER PRIMARY KEY,"
                    + "LOGINROLES_DESCRIPTION VARCHAR(255));");

            statement.execute("create table PUBLIC.CUSTOMER ("
                    + "USER_NAME VARCHAR(255) PRIMARY KEY,"
                    + "PASSWORD VARCHAR(255),"
                    + "NAME VARCHAR(255),"
                    + "TELEPHONE INTEGER,"
                    + "RESIDENCE VARCHAR(255),"
                    + "LOGINROLES_ID INTEGER,"
                    + "FOREIGN KEY(LOGINROLES_ID) REFERENCES LOGINROLES(LOGINROLES_ID));");

            statement.execute("create table PUBLIC.TYPE ("
                    + "TYPE_UID INTEGER PRIMARY KEY,"
                    + "TYPE_DESCRIPTION VARCHAR(255));");

            statement.execute("create table PUBLIC.DEVICE ("
                    + "SERIAL_NUMBER INTEGER PRIMARY KEY,"
                    + "USER_NAME VARCHAR(255),"
                    + "MODEL VARCHAR(255),"
                    + "FRIENDLY_NAME VARCHAR(255),"
                    + "TYPE_UID INTEGER,"
                    + "FOREIGN KEY(USER_NAME) REFERENCES CUSTOMER(USER_NAME),"
                    + "FOREIGN KEY(TYPE_UID) REFERENCES TYPE(TYPE_UID));");

            statement.execute("create table PUBLIC.PROGRAM ("
                    + "PROGRAM_ID INTEGER PRIMARY KEY,"
                    + "PROGRAM_DESCRIPTION VARCHAR(255));");

            statement.execute("create table PUBLIC.DEVICE_USAGE ("
                    + "TIME TIMESTAMP,"
                    + "SERIAL_NUMBER INTEGER,"
                    + "PROGRAM_ID INTEGER,"
                    + "WATER_TEMPERATURE INTEGER,"
                    + "FOREIGN KEY(PROGRAM_ID) REFERENCES PROGRAM(PROGRAM_ID),"
                    + "FOREIGN KEY(SERIAL_NUMBER) REFERENCES DEVICE(SERIAL_NUMBER));");

            //initialize type table
            statement.execute("insert into PUBLIC.LOGINROLES VALUES("
                    + "1,"
                    + "'admin');");

            statement.execute("insert into PUBLIC.LOGINROLES VALUES("
                    + "2,"
                    + "'user');");

            statement.execute("insert into PUBLIC.TYPE VALUES("
                    + "1,"
                    + "'Maquina de Lavar Roupa');");

            statement.execute("insert into PUBLIC.TYPE VALUES("
                    + "2,"
                    + "'Maquina de Lavar Loiça');");

            //initialize program table
            statement.execute("insert into PUBLIC.PROGRAM VALUES("
                    + "1,"
                    + "'Enxaguar');");

            statement.execute("insert into PUBLIC.PROGRAM VALUES("
                    + "2,"
                    + "'Normal');");

            statement.execute("insert into PUBLIC.PROGRAM VALUES("
                    + "3,"
                    + "'Muita Sujidade');");

            statement.execute("insert into PUBLIC.PROGRAM VALUES("
                    + "4,"
                    + "'Eco');");

            statement.execute("insert into PUBLIC.CUSTOMER VALUES("
                    + "'quim',"
                    + "'1',"
                    + "'quim',"
                    + "32295,"
                    + "'LinuxLandia',"
                    + "2);");

            statement.execute("insert into PUBLIC.CUSTOMER VALUES("
                    + "'Joao',"
                    + "'ola1',"
                    + "'Joao',"
                    + "32497,"
                    + "'AppleLandia',"
                    + "1);");

            statement.execute("insert into PUBLIC.CUSTOMER VALUES("
                    + "'renato',"
                    + "'1',"
                    + "'renato',"
                    + "32,"
                    + "'MicrosoftLandia',"
                    + "1);");

            statement.execute("insert into PUBLIC.DEVICE VALUES("
                    + "1,"
                    + "'renato',"
                    + "'Samsung',"
                    + "'Zambsung',"
                    + "1);");

            Date date = new Date();
            statement.execute("insert into PUBLIC.DEVICE_USAGE VALUES("
                    + "'" + new Timestamp(date.getTime()) + "',"
                    + "1,"
                    + "1,"
                    + "12);");

            //--------------------------------------------------------------------------------------------------------------------
            System.out.println("Database has been created");

            closeConnection();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /*
     This method inserts a new customer
     Receives the userName/name/telephone(int)/residence
     Returns a boolean
     */
    public boolean InsertCustomer(String userName, String name, int telephone, String residence, int loginRoles, String password) {
        try {
            openConnection();
            PreparedStatement preparedStatement = this.connection.prepareStatement("INSERT INTO PUBLIC.CUSTOMER("
                    + " USER_NAME,"
                    + " NAME,"
                    + " PASSWORD,"
                    + " TELEPHONE,"
                    + " RESIDENCE,"
                    + "LOGINROLES_ID)  VALUES (?,?,?,?,?,?)");
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, password);
            preparedStatement.setInt(4, telephone);
            preparedStatement.setString(5, residence);
            preparedStatement.setInt(6, loginRoles);
            preparedStatement.execute();
            closeConnection();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return false;
    }

    public boolean updateCustomer(String userName, int loginRole) {
        try {
            openConnection();
            statement.executeUpdate("UPDATE PUBLIC.CUSTOMER SET LOGINROLES_ID=" + loginRole + " WHERE USER_NAME = '" + userName + "'");
            closeConnection();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return false;
    }

    /*
     This method associates a device to a user
     Receives serialNumber/userName/model/friendlyName/deviceType(int)
     Returns a boolean
     */
    public boolean AssociateDevice(int serialNumber, String userName, String model, String friendlyName, int deviceType) {
        try {
            openConnection();
            PreparedStatement preparedStatement = this.connection.prepareStatement("INSERT INTO PUBLIC.DEVICE("
                    + " SERIAL_NUMBER,"
                    + " USER_NAME,"
                    + " MODEL,"
                    + " FRIENDLY_NAME,"
                    + " TYPE_UID)  VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1, serialNumber);
            preparedStatement.setString(2, userName);
            preparedStatement.setString(3, model);
            preparedStatement.setString(4, friendlyName);
            preparedStatement.setInt(5, deviceType);
            preparedStatement.execute();
            closeConnection();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return false;
    }

    /*
     This method returns all devices associated to an user
     Receives the userName
     Returns an ArrayList with structure: SerialNumber/FriendlyName
     */
    public ArrayList<String> getMyDevices(String userName) {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT SERIAL_NUMBER, FRIENDLY_NAME FROM PUBLIC.DEVICE WHERE USER_NAME = '" + userName + "'")) {
            ArrayList<String> myDevices = new ArrayList<>();
            while (rs.next()) {
                myDevices.add(rs.getString(1));
                myDevices.add(rs.getString(2));
            }
            closeConnection();
            return myDevices;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method returns all types of devices
     Returns an ArrayList with structure: Type_UID/TypeDescription
     */
    public ArrayList<String> getDeviceTypes() {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT * FROM PUBLIC.TYPE")) {
            ArrayList<String> myTypes = new ArrayList<>();
            while (rs.next()) {
                myTypes.add(rs.getString(1));
                myTypes.add(rs.getString(2));
            }
            closeConnection();
            return myTypes;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method returns all program types
     Returns an ArrayList with structure: PROGRAM_ID/PROGRAM_DESCRIPTION
     */
    public ArrayList<String> getProgams() {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT * FROM PUBLIC.PROGRAM")) {
            ArrayList<String> myProgramas = new ArrayList<>();
            while (rs.next()) {
                myProgramas.add(rs.getString(1));
                myProgramas.add(rs.getString(2));
            }
            closeConnection();
            return myProgramas;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method returns a device friendly name
     Receives the serial number' device
     Returns a string
     */
    public String getMyDeviceFriendlyName(String serialNumber) {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT FRIENDLY_NAME FROM PUBLIC.DEVICE WHERE SERIAL_NUMBER = " + serialNumber)) {
            rs.next();
            String myFriendlyName = rs.getString(1);
            closeConnection();
            return myFriendlyName;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method insert a new device usage
     Receives the serial number' device/program/water/temperature
     Returns a boolean
     */
    public boolean operateDevice(int serialNumber, int programID, int temperature) {
        try {
            openConnection();
            PreparedStatement preparedStatement = this.connection.prepareStatement("INSERT INTO PUBLIC.DEVICE_USAGE("
                    + " TIME,"
                    + " SERIAL_NUMBER,"
                    + " PROGRAM_ID,"
                    + " WATER_TEMPERATURE)  VALUES (?,?,?,?)");
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.UK);
            java.util.Date date = Calendar.getInstance().getTime();
            preparedStatement.setTimestamp(1, new Timestamp((formatter.parse(formatter.format(date))).getTime()));
            preparedStatement.setInt(2, serialNumber);
            preparedStatement.setInt(3, programID);
            preparedStatement.setInt(4, temperature);
            preparedStatement.execute();
            closeConnection();
            return true;
        } catch (SQLException | ParseException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return false;
    }

    /*
     This method returns the usage of a specific device
     Receives the serial number' device
     Returns an ArrayList with structure: Time/Program/WaterTemperature
     */
    public ArrayList<String> getDeviceUsages(String serialNumber) {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT TIME, PROGRAM_DESCRIPTION, WATER_TEMPERATURE "
                + "FROM DEVICE_USAGE, DEVICE, PROGRAM "
                + "WHERE DEVICE_USAGE.SERIAL_NUMBER = DEVICE.SERIAL_NUMBER "
                + "AND PROGRAM.PROGRAM_ID = DEVICE_USAGE.PROGRAM_ID "
                + "AND DEVICE_USAGE.SERIAL_NUMBER = '" + serialNumber + "'")) {
            ArrayList<String> myUsages = new ArrayList<>();
            while (rs.next()) {
                myUsages.add(rs.getString(1));
                myUsages.add(rs.getString(2));
                myUsages.add(rs.getString(3));
            }
            closeConnection();
            return myUsages;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    public ArrayList<RolesModel> getALLRoles() {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT * FROM PUBLIC.LOGINROLES")) {
            ArrayList<RolesModel> myTypes = new ArrayList<>();
            while (rs.next()) {
                myTypes.add(new RolesModel(rs.getString(2), rs.getString(1)));
            }
            closeConnection();
            return myTypes;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    public String getRole(String userName, String pw) {
        openConnection();
        try (ResultSet rs = statement.executeQuery(
                "SELECT LOGINROLES_DESCRIPTION FROM PUBLIC.CUSTOMER, PUBLIC.LOGINROLES WHERE CUSTOMER.PASSWORD = '" + pw + "' AND CUSTOMER.USER_NAME = '" + userName + "' AND LOGINROLES.LOGINROLES_ID = CUSTOMER.LOGINROLES_ID"
        )) {
            rs.next();
            String role = rs.getString(1);
            closeConnection();
            return role;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method returns the usage of a user
     Receives the userName
     Returns an ArrayList with structure: Time/Serial_Number/DeviceType/Program/WaterTemperature
     */
    public ArrayList<String> getCustomerUsages(String userName) {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT TIME, DEVICE.SERIAL_NUMBER, TYPE_DESCRIPTION, PROGRAM_DESCRIPTION, WATER_TEMPERATURE "
                + "FROM DEVICE_USAGE, DEVICE, TYPE, PROGRAM "
                + "WHERE DEVICE_USAGE.SERIAL_NUMBER = DEVICE.SERIAL_NUMBER "
                + "AND PROGRAM.PROGRAM_ID = DEVICE_USAGE.PROGRAM_ID "
                + "AND DEVICE.TYPE_UID = TYPE.TYPE_UID "
                + "AND DEVICE.USER_NAME = '" + userName + "'")) {
            ArrayList<String> myUsages = new ArrayList<>();
            while (rs.next()) {
                myUsages.add(rs.getString(1));
                myUsages.add(rs.getString(2));
                myUsages.add(rs.getString(3));
                myUsages.add(rs.getString(4));
                myUsages.add(rs.getString(5));
            }
            closeConnection();
            return myUsages;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    /*
     This method returns the usage of all devices with the same type
     Receives the typeID
     Returns an ArrayList with structure: Time/Serial_Number/user_name/DeviceType/WaterTemperature
     */
    public ArrayList<String> getDeviceTypeUsages(int typeID) {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT TIME, DEVICE.SERIAL_NUMBER, USER_NAME, PROGRAM_DESCRIPTION, WATER_TEMPERATURE "
                + "FROM DEVICE_USAGE, DEVICE, TYPE, PROGRAM "
                + "WHERE DEVICE_USAGE.SERIAL_NUMBER = DEVICE.SERIAL_NUMBER "
                + "AND PROGRAM.PROGRAM_ID = DEVICE_USAGE.PROGRAM_ID "
                + "AND DEVICE.TYPE_UID = " + typeID)) {
            ArrayList<String> myUsages = new ArrayList<>();
            while (rs.next()) {
                myUsages.add(rs.getString(1));
                myUsages.add(rs.getString(2));
                myUsages.add(rs.getString(3));
                myUsages.add(rs.getString(4));
                myUsages.add(rs.getString(5));
            }
            closeConnection();
            return myUsages;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

    public ArrayList<TypeModel> getallTypes() {
        openConnection();
        try (ResultSet rs = statement.executeQuery("SELECT * FROM PUBLIC.TYPE")) {
            ArrayList<TypeModel> myTypes = new ArrayList<>();
            while (rs.next()) {
                myTypes.add(new TypeModel(Integer.parseInt(rs.getString(1)), rs.getString(2)));
            }
            closeConnection();
            return myTypes;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeConnection();
        return null;
    }

}
