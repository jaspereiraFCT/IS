/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Joaquim
 */
public class CustomersUsages {
   
    private String date;
    private int serial_number;
    private String program_description;
    private String type_description;

    public CustomersUsages(String date, int serial_number, String program_description, String type_description, int temp) {
        this.date = date;
        this.serial_number = serial_number;
        this.program_description = program_description;
        this.type_description = type_description;
        this.temp = temp;
    }

    public void setType_description(String type_description) {
        this.type_description = type_description;
    }
    private int temp;

    public String getType_description() {
        return type_description;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setSerial_number(int serial_number) {
        this.serial_number = serial_number;
    }

    public void setProgram_description(String program_description) {
        this.program_description = program_description;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public String getDate() {
        return date;
    }

    public int getSerial_number() {
        return serial_number;
    }

    public String getProgram_description() {
        return program_description;
    }

    public int getTemp() {
        return temp;
    }
}
