/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Joaquim
 */
public class MyDevices {
    private String frindlyName;
    private String serialNumber;

    public MyDevices(String frindlyName, String serialNumber) {
        this.frindlyName = frindlyName;
        this.serialNumber = serialNumber;
    }

    public String getFrindlyName() {
        return frindlyName;
    }

    public void setFrindlyName(String frindlyName) {
        this.frindlyName = frindlyName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
}
