/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServices;

import DataBase.DataBaseManagement;
import Models.TypeModel;
import java.util.ArrayList;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author Joaquim
 */
@WebService(serviceName = "ClientWS")
@Stateless()
public class ClientWS {

    private final DataBaseManagement db = new DataBaseManagement();

    @WebMethod(operationName = "getMyDevices")
    public ArrayList<String> getMyDevices(@WebParam(name = "userName") String userName) {
        return db.getMyDevices(userName);
    }

    @WebMethod(operationName = "getallTypes")
    public ArrayList<TypeModel> getallTypes() {
        return db.getallTypes();
    }

    @WebMethod(operationName = "operateDevice")
    public boolean operateDevice(@WebParam(name = "serialNumber") int serialNumber, @WebParam(name = "programID") int programID, @WebParam(name = "temperature") int temperature) {
        webServicesClientOperation.OperationWS_Service service = new webServicesClientOperation.OperationWS_Service();
        webServicesClientOperation.OperationWS port = service.getOperationWSPort();
        if (port.operate(serialNumber, programID, temperature)) {
            return db.operateDevice(serialNumber, programID, temperature);
        }
        return false;
    }

    @WebMethod(operationName = "AssociateDevice")
    public boolean AssociateDevice(int serialNumber, String userName, String model, String friendlyName, int deviceType) {
        return db.AssociateDevice(serialNumber, userName, model, friendlyName, deviceType);
    }

    @WebMethod(operationName = "getProgams")
    public ArrayList<String> getProgams() {
        return db.getProgams();
    }

}
