package WebServices;

import DataBase.DataBaseManagement;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

@WebService(serviceName = "LoginWS")
@Stateless()
public class LoginWS {

    private final DataBaseManagement db = new DataBaseManagement();

    @WebMethod(operationName = "InsertAdmin")
    public Boolean InsertAdmin(@WebParam(name = "userName") String userName, @WebParam(name = "name") String name, @WebParam(name = "telephone") int telephone, @WebParam(name = "residence") String residence, @WebParam(name = "password") String password) {
        return db.InsertCustomer(userName, name, telephone, residence, 1,password);
    }

    @WebMethod(operationName = "InsertCustomer")
    public Boolean InsertCustomer(@WebParam(name = "userName") String userName, @WebParam(name = "name") String name, @WebParam(name = "telephone") int telephone, @WebParam(name = "residence") String residence, @WebParam(name = "password") String password) {
        return db.InsertCustomer(userName, name, telephone, residence, 2,password);
    }

    @WebMethod(operationName = "UpdateCustomerToAdmin")
    public Boolean UpdateCustomerToAdmin(@WebParam(name = "userName") String userName) {
        return db.updateCustomer(userName, 1);
    }

    @WebMethod(operationName = "UpdateCustomerToUser")
    public Boolean UpdateCustomerToUser(@WebParam(name = "userName") String userName) {
        return db.updateCustomer(userName, 2);
    }

    @WebMethod(operationName = "getRole")
    public String getRole(@WebParam(name = "userName") String userName, @WebParam(name = "pw") String pw) {
        return db.getRole(userName, pw);
    }

    @WebMethod(operationName = "CreateDB")
    public boolean CreateDB() {
        return db.createDB();
    }

}
