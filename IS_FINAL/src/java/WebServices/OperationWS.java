/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServices;

import java.util.Random;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

/**
 *
 * @author Joaquim
 */
@WebService(serviceName = "OperationWS")
@Stateless()
public class OperationWS {

    /**
     * This is a sample web service operation
     *
     * @param Serial
     * @return
     */
    @WebMethod(operationName = "Operate")
    public boolean Operate(@WebParam(name = "serialNumber") int serialNumber, @WebParam(name = "programID") int programID, @WebParam(name = "temperature") int temperature) {
        Random ran = new Random();
        if ((ran.nextInt() % 2) == 0) {
            System.out.println("Start Operation in S/N: " + serialNumber + " with programID : " + programID + " with temperature : " + temperature);
            return true;
        } else {
            System.out.println("!!! CAN T Start Operation in S/N: " + serialNumber + " with programID : " + programID + " with temperature : " + temperature);
            return false;
        }

    }
}
